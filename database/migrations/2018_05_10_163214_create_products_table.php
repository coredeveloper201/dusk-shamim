<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 500);
            $table->string('sku', 500);
            $table->string('image', 500);
            $table->string('height', 500);
            $table->string('weight', 500);
            $table->string('base', 500);
            $table->string('socket', 500);
            $table->string('wattage', 500);
            $table->string('price', 500);
            $table->string('availability', 500);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
