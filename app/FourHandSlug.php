<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FourHandSlug extends Model
{
    //
    protected $table = 'fourhandslugs';
}
