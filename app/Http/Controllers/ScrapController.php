<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Laravel\Dusk\Browser;
use Laravel\Dusk\Chrome\ChromeProcess;
use Facebook\WebDriver\Chrome\ChromeOptions;
use Facebook\WebDriver\Remote\DesiredCapabilities;
use Facebook\WebDriver\Remote\RemoteWebDriver;

class ScrapController extends Controller
{
    public function scrap()
    {
    	$process = (new ChromeProcess)->toProcess();

        $process->start();

        $options = (new ChromeOptions)->addArguments(
            ['--disable-gpu', '--headless']
        );

        $capabilities = DesiredCapabilities::chrome()->setCapability(ChromeOptions::CAPABILITY, $options);

        $driver = retry(5, function () use($capabilities) {
            return RemoteWebDriver::create('http://localhost:9515', $capabilities);
        }, 50);

        $browser = new Browser($driver);
        $browser->maximize();

        $browser->visit('https://www.visualcomfort.com/trade-portal/')
                ->type('#ContentPlaceHolderDefault_Content_Login_3_TxtLogin', '12720CD')
                ->type('#ContentPlaceHolderDefault_Content_Login_3_TxtPassword', 'Success7')
                ->press('#ContentPlaceHolderDefault_Content_Login_3_BtnLogin')
                ->assertSee('About')
                ->visit('https://www.visualcomfort.com/shop-by/')
                ->visit('https://www.visualcomfort.com/categories')
                ->assertSee('About');

                //Getting category links to iterate through

                $elements = $browser->elements('table a');

                $data = [];

                foreach($elements as $element)
                {
                    $data[] = trim($element->getAttribute('href'));
                }

                //Getting products links for the first category

                $single = [];
                $browser->visit($data[0]);
                $elements = $browser->elements('#dvScroll a');
                foreach ($elements as $elems) {
                    $single[] = trim($elems->getAttribute('href'));
                }

                // $info['description'] = $browser
                // $info['SKU'] = $browser
                // $info['others'] = $browser->text('table');
                // $info['availability'] = $browser
                // $info['backorder status'] = $browser
                // $info['image URL'] = $browser
                // $info['dimensions'] = $browser
                // $info['weight'] = $browser

        var_dump(array_unique($data, SORT_STRING));

        print_r('test drive');
        print_r($single);

        $browser->quit();
        $process->stop();
        session()->flush();
    }
}
