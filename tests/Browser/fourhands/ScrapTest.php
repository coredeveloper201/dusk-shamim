<?php

namespace Tests\Browser\fourhands;

use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use File;
use Image;
use App\FourHandSlug as FourHandSlug;
use App\FourHandProduct as FourHandProduct;

class ScrapTest extends DuskTestCase
{
    /**
     * A Dusk test example.
     *
     * @return void
     */
    public function testExample()
    {
        $this->browse(function (Browser $browser) {

            // get the links from the database
            $fourHandslugs = FourHandSlug::where('status', false)
                               ->take(1)
                               ->get();

           // get links from database that are not visited and visit them
           foreach($fourHandslugs as $fourHandslug):

               // $master_data    = [];
               $secondary_data = [];
               $temp_data      = [];

               // browse that link and login
               $browser->visit('https://marketplace.fourhands.com/login')
                           ->type('#p_lt_zoneContent_Marketplace_LogonForm_logonFormInner_LogonForm_loginElem_UserName', '75839')
                           ->type('#p_lt_zoneContent_Marketplace_LogonForm_logonFormInner_LogonForm_loginElem_Password', 'Success7')
                           ->press('#p_lt_zoneContent_Marketplace_LogonForm_logonFormInner_LogonForm_loginElem_btnLogon')
                           ->visit($fourHandslug->links)
                           ->assertSee('BUSINESS');

               // get product image
               foreach($browser->elements('img#zoomable') as $element):
                   $secondary_data['image'] = trim($element->getAttribute('data-zoom-image'));
                   break;
               endforeach;
               // ends

               // get product slug
               $secondary_data['slug'] = $fourHandslug->slug;
               // ends

               // get product name
               foreach($browser->elements('div.xl.caps') as $element):
                   $secondary_data['name'] = stripslashes( strip_tags( trim($element->getAttribute('innerHTML')) ) );
                   break;
               endforeach;
               // ends

               // get product sku
               $secondary_data['sku'] = $fourHandslug->slug;
               // ends

               // get product availability
               foreach($browser->elements('div[data-bind="visible: availabilityDescription"]') as $element):
                   $secondary_data['availability'] = trim($element->getAttribute('innerHTML'));
                   break;
               endforeach;
               // ends

               // get product overview
               foreach($browser->elements('div#sectionOverview') as $element):
                   $secondary_data['overview'] = trim($element->getAttribute('innerHTML'));
                   break;
               endforeach;
               // ends

               // get product details
               foreach($browser->elements('div#sectionDetails') as $element):
                   $secondary_data['details'] = trim($element->getAttribute('innerHTML'));
                   break;
               endforeach;
               // ends

               // get product specification
               foreach($browser->elements('div#sectionCollection') as $element):
                   $secondary_data['specification'] = trim($element->getAttribute('innerHTML'));
                   break;
               endforeach;
               // ends

               // get product price
               foreach($browser->elements('span.xl') as $element):
                   $secondary_data['price'] = trim($element->getAttribute('innerHTML'));
                   break;
               endforeach;
               // ends


               if( !empty($secondary_data) ):

                   // save extracted data to the database and image to image folder and mark the link as visited

                   // get server image
                   $path           = $secondary_data['image'];
                   $filename       = basename($path);
                   $dirname        = 'fourhand_images/'.$fourHandslug->slug.'/';
                   $public_dirname = '/public/fourhand_images/'.$fourHandslug->slug.'/'.$filename;
                   $directory_path = public_path($dirname . $filename);

                   // path does not exist
                   if(!File::exists(public_path($dirname))):
                       File::makeDirectory(public_path($dirname), 0755, true, true);
                   endif;
                   // ends

                   $fourhandproduct                = new FourHandProduct;
                   $fourhandproduct->name          = $secondary_data['name'];
                   $fourhandproduct->slug          = $secondary_data['slug'];
                   $fourhandproduct->sku           = $secondary_data['sku'];
                   $fourhandproduct->image         = $public_dirname;
                   $fourhandproduct->overview      = $secondary_data['overview'];
                   $fourhandproduct->details       = $secondary_data['details'];
                   $fourhandproduct->specification = $secondary_data['specification'];
                   $fourhandproduct->availability  = $secondary_data['availability'];
                   $fourhandproduct->price         = $secondary_data['price'];

                   // if product saved then save the image and update the link
                   if( $fourhandproduct->save() ):
                       $fourHandslug->status = true;
                       $fourHandslug->save();
                       Image::make($path)->save($directory_path);
                   endif;

               endif;

               $browser->visit('https://marketplace.fourhands.com/account/logout');

           endforeach;
        });
    }
}
