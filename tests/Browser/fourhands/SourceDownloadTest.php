<?php

namespace Tests\Browser\fourhands;

use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use App\FourHandSlug as FourHandSlug;

class SourceDownloadTest extends DuskTestCase
{
    /**
     * A Dusk test example.
     *
     * @return void
     */
    public function testExample()
    {
        $this->browse(function (Browser $browser) {

            $source_list = [
                'https://marketplace.fourhands.com/new',
                'https://marketplace.fourhands.com/shop/living-room',
                'https://marketplace.fourhands.com/shop/dining-room',
                'https://marketplace.fourhands.com/shop/bedroom',
                'https://marketplace.fourhands.com/shop/office',
                'https://marketplace.fourhands.com/outdoor',
                'https://marketplace.fourhands.com/shop/lighting',
                'https://marketplace.fourhands.com/shop/decor',
                'https://marketplace.fourhands.com/sale',
                'https://marketplace.fourhands.com/collections/abbott',
                'https://marketplace.fourhands.com/collections/bina',
                'https://marketplace.fourhands.com/collections/carnegie',
                'https://marketplace.fourhands.com/collections/constantine',
                'https://marketplace.fourhands.com/collections/hughes',
                'https://marketplace.fourhands.com/collections/irish-coast',
                'https://marketplace.fourhands.com/collections/irondale',
                'https://marketplace.fourhands.com/collections/kensington',
                'https://marketplace.fourhands.com/collections/marlow',
                'https://marketplace.fourhands.com/collections/post-rail',
                'https://marketplace.fourhands.com/collections/rockwell',
                'https://marketplace.fourhands.com/collections/settler',
                'https://marketplace.fourhands.com/collections/sierra',
                'https://marketplace.fourhands.com/collections/theory',
                'https://marketplace.fourhands.com/collections/van-thiel-co',
                'https://marketplace.fourhands.com/collections/wesson',
            ];

            foreach($source_list as $source):

                $links = [];
                $product_links = [];

                // browse that link and login
                $browser->visit('https://marketplace.fourhands.com/login')
                            ->type('#p_lt_zoneContent_Marketplace_LogonForm_logonFormInner_LogonForm_loginElem_UserName', '75839')
                            ->type('#p_lt_zoneContent_Marketplace_LogonForm_logonFormInner_LogonForm_loginElem_Password', 'Success7')
                            ->press('#p_lt_zoneContent_Marketplace_LogonForm_logonFormInner_LogonForm_loginElem_btnLogon')
                            ->visit($source)
                            ->assertSee('BUSINESS');

                // scroll the ajax pages until gets end
                $lenOfPage = $browser->driver->executeScript("window.scrollTo(0, document.body.scrollHeight);var lenOfPage=document.body.scrollHeight;return lenOfPage;");
                $match = false;
                while($match == false):
                    $lastCount = $lenOfPage;
                    sleep(4);
                    $lenOfPage = $browser->driver->executeScript("window.scrollTo(0, document.body.scrollHeight);var lenOfPage=document.body.scrollHeight;return lenOfPage;");
                    if($lastCount == $lenOfPage):
                        $match = true;
                    endif;
                endwhile;
                // ends

                // get all new product links
                foreach($browser->elements('a.product__img') as $element):
                    $links[] = trim($element->getAttribute('href'));
                endforeach;
                // ends

                // gain the product list to import
                if( !empty($links) ):
                    foreach($links as $link):
                        $product_links[] = [
                            'slug' => str_replace( array('product/'), '', trim( parse_url( trim( strip_tags( stripslashes($link) ) ), PHP_URL_PATH ), '/' ) ),
                            'link' => trim( strip_tags( stripslashes($link) ) ),
                        ];
                    endforeach;
                endif;
                // ends

                // save the list to database if not exists
                if( !empty($product_links) ):

                    foreach($product_links as $fields):

                        if( FourHandSlug::where('slug', $fields["slug"])->first() ):
                            echo "Reject Record Exists >> " . $fields["slug"] . PHP_EOL;
                            continue;
                        endif;

                        $fourhandslug         = new FourHandSlug;
                        $fourhandslug->slug   = $fields['slug'];
                        $fourhandslug->links  = $fields['link'];
                        $fourhandslug->status = false;

                        if( $fourhandslug->save() ):
                            echo PHP_EOL;
                            echo "Record Added >> ".$fields['slug'];
                            echo PHP_EOL;
                        else:
                            echo "Error >> " . $fields['slug'];
                            echo PHP_EOL;
                        endif;

                    endforeach;

                endif;
                // ends

                $browser->visit('https://marketplace.fourhands.com/account/logout');

            endforeach;

        });
    }
}
