<?php

namespace Tests\Browser\hookerfurniture;

use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use File;
use Image;
use App\HookerLink as HookerLink;
use App\HookerProduct as HookerProduct;

class ScrapTest extends DuskTestCase
{
    /**
     * A Dusk test example.
     *
     * @return void
     */
    public function testExample()
    {
        $this->browse(function (Browser $browser) {

            // get the links from the database
            $hookerlinks = HookerLink::where('status', false)
                           ->take(1)
                           ->get();

            // get links from database that are not visited and visit them
            foreach($hookerlinks as $hookerlink):

                // $master_data    = [];
                $secondary_data = [];
                $temp_data      = [];

                // browse that link and login
                $browser->visit($hookerlink->links)
                            ->assertSee('Hooker');

                // get product image
                foreach($browser->elements('a#newItemImageControl_lnkMagicZoom') as $element):
                    $url = explode("?", trim($element->getAttribute('href')));
                    $url = $url[0];
                    $secondary_data['image'] = $url;
                    break;
                endforeach;
                // ends

                // get product link
                $secondary_data['link'] = $hookerlink->links;
                // ends

                // get product name
                foreach($browser->elements('h1.ProductDescriptionHeading') as $element):
                    $secondary_data['name'] = trim($element->getAttribute('innerHTML'));
                    break;
                endforeach;
                // ends

                // get product bibilo
                foreach($browser->elements('span.ProductInfoSpanValueCollectionFeatures') as $element):
                    $secondary_data['bibilo'] = trim($element->getAttribute('innerHTML'));
                    break;
                endforeach;
                // ends

                // get product sku
                foreach($browser->elements('span.ProductInfoSpanValueSKU') as $element):
                    $secondary_data['sku'] = trim($element->getAttribute('innerHTML'));
                    break;
                endforeach;
                // ends

                // get product dimensions
                foreach($browser->elements('div.ProductInfoDimensions') as $element):
                    $secondary_data['dimensions'] = trim($element->getAttribute('innerHTML'));
                    break;
                endforeach;
                // ends

                // get product availability
                foreach($browser->elements('div.ProductInfoAvailibility') as $element):
                    $secondary_data['availability'] = trim($element->getAttribute('innerHTML'));
                    break;
                endforeach;
                // ends

                // get product features
                foreach($browser->elements('div.ProductInfoDesignElementsFeatures') as $element):
                    $secondary_data['features'] = trim($element->getAttribute('innerHTML'));
                    break;
                endforeach;
                // ends

                // get product details
                foreach($browser->elements('div.ProductInfoOtherDetails') as $element):
                    $secondary_data['details'] = trim($element->getAttribute('innerHTML'));
                    break;
                endforeach;
                // ends

                if( !empty($secondary_data) ):

                    // save extracted data to the database and image to image folder and mark the link as visited

                    // get server image
                    $path           = $secondary_data['image'];
                    $filename       = basename($path);
                    $folder         = md5($hookerlink->id);
                    $dirname        = 'hookerfurniture_images/'.$folder.'/';
                    $public_dirname = '/public/hookerfurniture_images/'.$folder.'/'.$filename;
                    $directory_path = public_path($dirname . $filename);

                    // path does not exist
                    if(!File::exists(public_path($dirname))):
                        File::makeDirectory(public_path($dirname), 0755, true, true);
                    endif;
                    // ends

                    $hookerproduct                  = new HookerProduct;
                    $hookerproduct->link            = $secondary_data['link'];
                    $hookerproduct->name            = $secondary_data['name'];
                    $hookerproduct->image           = $public_dirname;
                    $hookerproduct->bibilo          = $secondary_data['bibilo'];
                    $hookerproduct->sku             = $secondary_data['sku'];
                    $hookerproduct->dimensions      = $secondary_data['dimensions'];
                    $hookerproduct->availability    = $secondary_data['availability'];
                    $hookerproduct->features        = $secondary_data['features'];
                    $hookerproduct->details         = $secondary_data['details'];

                    // if product saved then save the image and update the link
                    if( $hookerproduct->save() ):
                        $hookerlink->status = true;
                        $hookerlink->save();
                        Image::make($path)->save($directory_path);
                    endif;

                endif;

            endforeach;

        });
    }
}
