<?php

namespace Tests\Browser\hookerfurniture;

use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use App\HookerLink as HookerLink;

class SourceDownloadTest extends DuskTestCase
{
    /**
     * A Dusk test example.
     *
     * @return void
     */
    public function testExample()
    {
        $this->browse(function (Browser $browser) {

            $source_list = [
                'https://www.hookerfurniture.com/itembrowser.aspx?action=attributes&Department=Home%20Entertainment&Type=Entertainment%20Centers&SubType=TV%20Stands&viewall=true',
                'https://www.hookerfurniture.com/itembrowser.aspx?action=attributes&Department=Home%20Entertainment&Type=Entertainment%20Centers&SubType=Centers&viewall=true',
                'https://www.hookerfurniture.com/itembrowser.aspx?action=attributes&Department=Home%20Entertainment&Type=Entertainment%20Centers&SubType=Wall%20Units&viewall=true',
                'https://www.hookerfurniture.com/itembrowser.aspx?action=attributes&Type=Chairs&SubType=Accent%20Chairs&viewall=true',
                'https://www.hookerfurniture.com/itembrowser.aspx?action=attributes&Type=Tables&SubType=Accent%20Tables&viewall=true',
                'https://www.hookerfurniture.com/itembrowser.aspx?action=attributes&Room=Living%20Room&Type=Cabinets&SubType=Credenza%20Cabinets&viewall=true',
                'https://www.hookerfurniture.com/itembrowser.aspx?action=attributes&Room=Living%20Room&Type=Chests&SubType=Decorative%20Chests&viewall=true',
                'https://www.hookerfurniture.com/itembrowser.aspx?action=attributes&Category=Accessories&Type=Mirrors&viewall=true',
                'https://www.hookerfurniture.com/itembrowser.aspx?action=attributes&Room=Living%20Room&Type=Tables&SubType=Console%20Tables&viewall=true',
                'https://www.hookerfurniture.com/itembrowser.aspx?action=attributes&Room=Dining%20Room&Type=Bars&viewall=true',
                'https://www.hookerfurniture.com/itembrowser.aspx?action=attributes&Brand=Hooker%20Furniture&Department=Bathroom&Type=Sink%20Cabinets&viewall=true',
                'https://www.hookerfurniture.com/itembrowser.aspx?action=attributes&Department=Home%20Office&Type=Desks&viewall=true',
                'https://www.hookerfurniture.com/itembrowser.aspx?action=attributes&Department=Home%20Office&Type=Chairs&SubType=Desk%20Chairs&viewall=true',
                'https://www.hookerfurniture.com/itembrowser.aspx?action=attributes&Department=Home%20Office&Type=Bookcases&viewall=true',
                'https://www.hookerfurniture.com/itembrowser.aspx?action=attributes&ItemType=Furniture&Department=Home%20Office&Type=Cabinets&SubType=File%2fStorage%20Cabinets&viewall=true',
                'https://www.hookerfurniture.com/itembrowser.aspx?action=attributes&Department=Home%20Office&Custom%20Type=Office%20Wall%20Systems&viewall=true',
                'https://www.hookerfurniture.com/itembrowser.aspx?action=attributes&Room=Bedroom&Type=Beds&viewall=true',
                'https://www.hookerfurniture.com/itembrowser.aspx?action=attributes&Room=Bedroom&Type=Nightstands&viewall=true',
                'https://www.hookerfurniture.com/itembrowser.aspx?action=attributes&Room=Bedroom&Type=Chests%20and%20Dressers&viewall=true',
                'https://www.hookerfurniture.com/itembrowser.aspx?action=attributes&Room=Bedroom&Type=Mirrors&viewall=true',
                'https://www.hookerfurniture.com/itembrowser.aspx?action=attributes&Room=Bedroom&Type=Armoire%20Cabinets&viewall=true',
                'https://www.hookerfurniture.com/itembrowser.aspx?action=attributes&Room=Bedroom&Type=Benches',
                'https://www.hookerfurniture.com/itembrowser.aspx?action=attributes&Deparment=Mattresses&Category=Mattresses&viewall=true',
                'https://www.hookerfurniture.com/itembrowser.aspx?action=attributes&Room=Dining%20Room&Type=Tables&SubType=Dining%20Tables&viewall=true',
                'https://www.hookerfurniture.com/itembrowser.aspx?action=attributes&Room=Dining%20Room&Type=Chairs&viewall=true',
                'https://www.hookerfurniture.com/itembrowser.aspx?action=attributes&Room=Dining%20Room&Type=Cabinets&SubType=Sideboard%20Cabinets&viewall=true',
                'https://www.hookerfurniture.com/itembrowser.aspx?action=attributes&Room=Dining%20Room&Type=Cabinets&SubType=China%20Cabinets&viewall=true',
                'https://www.hookerfurniture.com/itembrowser.aspx?action=attributes&Room=Dining%20Room&Type=Cabinets&SubType=Credenza%20Cabinets&viewall=true',
                'https://www.hookerfurniture.com/itembrowser.aspx?action=attributes&Room=Dining%20Room&Type=Stools&viewall=true',
                'https://www.hookerfurniture.com/itembrowser.aspx?action=attributes&Room=Dining%20Room&Type=Bars&viewall=true',
                'https://www.hookerfurniture.com/itembrowser.aspx?action=attributes&Brand=Hooker%20Furniture&Room=Living%20Room&Type=Sofas&viewall=true',
                'https://www.hookerfurniture.com/itembrowser.aspx?action=attributes&Brand=Hooker%20Furniture&Room=Living%20Room&Type=Chairs&Motion%20Type=Swivel&Motion%20Type=Stationary&viewall=true',
                'https://www.hookerfurniture.com/itembrowser.aspx?action=attributes&Brand=Hooker%20Furniture&Room=Living%20Room&Type=Sectionals&viewall=true',
                'https://www.hookerfurniture.com/itembrowser.aspx?action=attributes&Brand=Hooker%20Furniture&Room=Living%20Room&Type=Loveseats&viewall=true',
                'https://www.hookerfurniture.com/itembrowser.aspx?action=attributes&Room=Living%20Room&Type=Chairs&Motion%20Type=Reclining&viewall=true',
                'https://www.hookerfurniture.com/itembrowser.aspx?action=attributes&Brand=Hooker%20Furniture&Room=Living%20Room&Type=Ottomans&Type=Benches&viewall=true',
                'https://www.hookerfurniture.com/itembrowser.aspx?action=attributes&Room=Living%20Room&Type=Tables&SubType=Coffee%20Tables&viewall=true',
                'https://www.hookerfurniture.com/itembrowser.aspx?action=attributes&Room=Living%20Room&Type=Tables&SubType=End%20Tables&viewall=true',
                'https://www.hookerfurniture.com/itembrowser.aspx?action=attributes&Room=Living%20Room&Type=Tables&SubType=Console%20Tables&viewall=true',
                'https://www.hookerfurniture.com/itembrowser.aspx?action=attributes&Brand=Cynthia%20Rowley%20for%20Hooker%20Furniture&Room=Living%20Room&Type=Sofas&Type=Settees&Type=Chairs&Motion%20Type=Swivel&Motion%20Type=Stationary&Type=Sectionals&Type=Loveseats&Type=Ottomans&Type=Benches&viewall=true',
                'https://www.hookerfurniture.com/itembrowser.aspx?action=attributes&ItemType=Furniture&Collection=American%20Life%20Crafted&viewall=true',
                'https://www.hookerfurniture.com/itembrowser.aspx?action=attributes&Brand=Hooker%20Furniture&ItemType=Finish&viewall=true',
            ];

            foreach($source_list as $source):

                $links = [];
                $product_links = [];

                // browse that link
                $browser->visit($source)
                        ->assertSee('Hooker');

                // get all new product links
                foreach($browser->elements('div.ProductThumbnail a[target="_self"]') as $element):
                    $links[] = trim($element->getAttribute('href'));
                endforeach;
                // ends

                // gain the product list to import
                if( !empty($links) ):
                    foreach($links as $link):
                        $product_links[] = [
                            'link' => trim( strip_tags( stripslashes($link) ) ),
                        ];
                    endforeach;
                endif;
                // ends

                // save the list to database if not exists
                if( !empty($product_links) ):

                    foreach($product_links as $fields):

                        if( HookerLink::where('links', $fields["link"])->first() ):
                            echo "Reject Record Exists >> " . $fields["link"] . PHP_EOL;
                            continue;
                        endif;

                        $hookerlink         = new HookerLink;
                        $hookerlink->links  = $fields['link'];
                        $hookerlink->status = false;

                        if( $hookerlink->save() ):
                            echo PHP_EOL;
                            echo "Record Added >> ".$fields['link'];
                            echo PHP_EOL;
                        else:
                            echo "Error >> " . $fields['link'];
                            echo PHP_EOL;
                        endif;

                    endforeach;

                endif;
                // ends

            endforeach;

        });
    }
}
