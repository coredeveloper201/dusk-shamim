<?php

namespace Tests\Browser;

use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use File;
use Image;
use App\Link as Link;
use App\Product as Product;

class ScrapTest extends DuskTestCase
{
    /**
     * A Dusk test example.
     *
     * @return void
     */
    public function testExample()
    {
        $this->browse(function (Browser $browser) {

            // get the links from the database
            $links = Link::where('status', false)
                           ->take(3)
                           ->get();

            // get links from database that are not visited and visit them
            foreach($links as $link):

                // $master_data    = [];
                $secondary_data = [];
                $temp_data      = [];

                // browse that link and extract
                $browser->visit('https://www.visualcomfort.com/trade-portal/')
                            ->type('#ContentPlaceHolderDefault_Content_Login_3_TxtLogin', '12720CD')
                            ->type('#ContentPlaceHolderDefault_Content_Login_3_TxtPassword', 'Success7')
                            ->press('#ContentPlaceHolderDefault_Content_Login_3_BtnLogin')
                            ->visit($link->links)
                            ->assertSee('About');

                // get product image
                foreach($browser->elements('a.eLarge') as $element):
                    $secondary_data['image'] = trim($element->getAttribute('href'));
                    break;
                endforeach;
                // ends

                // get product name, description, SKU, price(s), availability, backorder status, dimensions, weight unfiltered
                foreach($browser->elements('p') as $element):
                    $temp_data[] = trim($element->getAttribute('innerHTML'));
                endforeach;

                if( !empty($temp_data) ):

                    $title = $sku = null;

                    foreach( $temp_data as $key => $data_row ):

                        // get product title and sku
                        if( strpos( $data_row, 'strong style="color:#333"' ) !== false ):
                            $title = explode('<br>', $data_row);
                            $secondary_data['name'] = trim( strip_tags( stripslashes( $title[0] ) ) );
                            $secondary_data['sku'] = str_replace( array('Item #'), '', trim( strip_tags( stripslashes( $title[1]  ) ) ) );
                        endif;
                        // ends

                        // get height
                        if( strpos( $data_row, 'Height' ) !== false ):
                            $secondary_data['height'] = str_replace( array('Height: '), '', trim( strip_tags( stripslashes( $data_row  ) ) ) );
                        endif;
                        // ends

                        // get width
                        if( strpos( $data_row, 'Width' ) !== false ):
                            $secondary_data['weight'] = str_replace( array('Width: '), '', trim( strip_tags( stripslashes( $data_row  ) ) ) );
                        endif;
                        // ends

                        // get base
                        if( strpos( $data_row, 'Base' ) !== false ):
                            $secondary_data['base'] = str_replace( array('Base: '), '', trim( strip_tags( stripslashes( $data_row  ) ) ) );
                        endif;
                        // ends

                        // get Socket
                        if( strpos( $data_row, 'Socket' ) !== false ):
                            $secondary_data['socket'] = str_replace( array('Socket: '), '', trim( strip_tags( stripslashes( $data_row  ) ) ) );
                        endif;
                        // ends

                        // get Wattage
                        if( strpos( $data_row, 'Wattage' ) !== false ):
                            $secondary_data['wattage'] = str_replace( array('Wattage: '), '', trim( strip_tags( stripslashes( $data_row  ) ) ) );
                        endif;
                        // ends

                        // get Price
                        if( strpos( $data_row, 'Price' ) !== false ):
                            $secondary_data['price'] = str_replace( array('Price: '), '', trim( strip_tags( stripslashes( $temp_data[$key+1]  ) ) ) );
                        endif;
                        // ends

                        // get Availability
                        if( strpos( $data_row, 'Availability' ) !== false ):
                            $secondary_data['availability'] = str_replace( array('Available Now'), '', trim( strip_tags( stripslashes( $temp_data[$key+1]  ) ) ) );
                        endif;
                        // ends

                    endforeach;

                    // $master_data[] = $secondary_data;
                    // $secondary_data = null;

                endif;

                if( !empty($secondary_data) ):

                    // save extracted data to the database and image to image folder and mark the link as visited

                    // get server image
                    $path           = $secondary_data['image'];
                    $filename       = basename($path);
                    $dirname        = 'visualcomfort_images/'.$link->productId.'/';
                    $public_dirname = '/public/visualcomfort_images/'.$link->productId.'/'.$filename;
                    $directory_path = public_path($dirname . $filename);

                    // path does not exist
                    if(!File::exists(public_path($dirname))):
                        File::makeDirectory(public_path($dirname), 0755, true, true);
                    endif;
                    // ends

                    $product                = new Product;
                    $product->productId     = $link->productId;
                    $product->name          = $secondary_data['name'];
                    $product->sku           = $secondary_data['sku'];
                    $product->image         = $public_dirname;
                    $product->height        = $secondary_data['height'];
                    $product->weight        = $secondary_data['weight'];
                    $product->base          = $secondary_data['base'];
                    $product->socket        = $secondary_data['socket'];
                    $product->wattage       = $secondary_data['wattage'];
                    $product->price         = $secondary_data['price'];
                    $product->availability  = $secondary_data['availability'];

                    // if product saved then save the image and update the link
                    if( $product->save() ):
                        $link->status = true;
                        $link->save();
                        Image::make($path)->save($directory_path);
                    endif;

                endif;

                $browser->visit('https://www.visualcomfort.com/logout');

            endforeach;

        });

    }
}
