<?php

namespace Tests\Browser;

use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use App\Link as Link;

class SourceDownloadTest extends DuskTestCase
{
    /**
     * A Dusk test example.
     *
     * @return void
     */
    public function testExample()
    {
        $this->browse(function (Browser $browser) {

            $links = [];
            $product_links = [];

            // browse that link and extract
            $browser->visit('https://www.visualcomfort.com/trade-portal/')
                        ->type('#ContentPlaceHolderDefault_Content_Login_3_TxtLogin', '12720CD')
                        ->type('#ContentPlaceHolderDefault_Content_Login_3_TxtPassword', 'Success7')
                        ->press('#ContentPlaceHolderDefault_Content_Login_3_BtnLogin')
                        ->visit('https://www.visualcomfort.com/categories/ProductList.aspx?Category=New')
                        ->assertSee('About');

            // get all new product links
            foreach($browser->elements('a') as $element):
                $links[] = trim($element->getAttribute('href'));
            endforeach;
            // ends

            // gain the product list to import
            if( !empty($links) ):

                foreach($links as $link):

                    if( strpos( $link, 'ItemCode' ) !== false ):

                        parse_str(parse_url($link, PHP_URL_QUERY), $output);

                        $product_links[] = [
                            'productId' => trim( strip_tags( stripslashes($output['ItemCode']) ) ),
                            'link'      => 'https://www.visualcomfort.com/categories/' . trim( strip_tags( stripslashes( str_replace(array('&amp;Category=New'), '', $link) ) ) ),
                        ];

                    else:
                        continue;
                    endif;

                endforeach;

            endif;
            // ends

            // save the list to database if not exists
            if( !empty($product_links) ):

                foreach ($product_links as $fields):

                    if( Link::where('productId', $fields["productId"])->first() ):
                        echo "Reject Record Exists >> " . $fields["productId"] . PHP_EOL;
                        continue;
                    endif;

                    $link               = new Link;
                    $link->productId    = $fields['productId'];
                    $link->link         = $fields['link'];
                    $link->status       = false;

                    if( $link->save() ):
                        echo PHP_EOL;
                        echo "Record Added >> ".$fields['productId'];
                        echo PHP_EOL;
                    else:
                        echo "Error >> " . $fields['productId'];
                        echo PHP_EOL;
                    endif;

                endforeach;

            endif;
            // ends

            $browser->visit('https://www.visualcomfort.com/logout');

        });
    }
}
