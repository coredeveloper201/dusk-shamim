<?php

namespace Tests\Browser;

use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use File;
use Image;
use App\Slug as Slug;
use App\Item as Item;

class ScrapTest extends DuskTestCase
{
    /**
     * A Dusk test example.
     *
     * @return void
     */
    public function testExample()
    {
        $this->browse(function (Browser $browser) {

            // get the links from the database
            $slugs = Slug::where('status', false)
                           ->take(1)
                           ->get();

           // get links from database that are not visited and visit them
           foreach($slugs as $slug):

               // $master_data    = [];
               $secondary_data = [];
               $temp_data      = [];

               // browse that link and login
               $browser->visit('https://www.classichome.com/')
                           ->press('a.login')
                           ->type('#mini-login', 'vendors@plaidfox.com')
                           ->type('#mini-password', 'Success7')
                           ->press('div.block-login button.button')
                           ->visit($slug->links)
                           ->assertSee('Products');

               // get product image
               foreach($browser->elements('a.MagicZoomPlus') as $element):
                   $secondary_data['image'] = trim($element->getAttribute('href'));
                   break;
               endforeach;
               // ends

               // get product slug
               $secondary_data['slug'] = $slug->slug;
               // ends

               // get product name
               foreach($browser->elements('h1') as $element):
                   $secondary_data['name'] = trim($element->getAttribute('innerHTML'));
                   break;
               endforeach;
               // ends

               // get product sku
               foreach($browser->elements('p.sku') as $element):
                   $secondary_data['sku'] = trim($element->getAttribute('innerHTML'));
                   break;
               endforeach;
               // ends

               // get product description
               foreach($browser->elements('div.description') as $element):
                   $secondary_data['description'] = trim($element->getAttribute('innerHTML'));
                   break;
               endforeach;
               // ends

               // get product specification
               foreach($browser->elements('div.specification') as $element):
                   $secondary_data['specification'] = trim($element->getAttribute('innerHTML'));
                   break;
               endforeach;
               // ends

               // get product availability
               foreach($browser->elements('div.available') as $element):
                   $secondary_data['availability'] = trim($element->getAttribute('innerHTML'));
                   break;
               endforeach;
               // ends

               // get product price
               foreach($browser->elements('span.price') as $element):
                   $secondary_data['price'] = trim($element->getAttribute('innerHTML'));
                   break;
               endforeach;
               // ends

               // get product sell_price
               foreach($browser->elements('span.price') as $element):
                   $secondary_data['sell_price'] = trim($element->getAttribute('innerHTML'));
                   break;
               endforeach;
               // ends


               if( !empty($secondary_data) ):

                   // save extracted data to the database and image to image folder and mark the link as visited

                   // get server image
                   $path           = $secondary_data['image'];
                   $filename       = basename($path);
                   $dirname        = 'classichome_images/'.$slug->slug.'/';
                   $public_dirname = '/public/classichome_images/'.$slug->productId.'/'.$filename;
                   $directory_path = public_path($dirname . $filename);

                   // path does not exist
                   if(!File::exists(public_path($dirname))):
                       File::makeDirectory(public_path($dirname), 0755, true, true);
                   endif;
                   // ends

                   $item                = new Item;
                   $item->name          = $secondary_data['name'];
                   $item->slug          = $secondary_data['slug'];
                   $item->sku           = $secondary_data['sku'];
                   $item->image         = $public_dirname;
                   $item->description   = $secondary_data['description'];
                   $item->specification = $secondary_data['specification'];
                   $item->availability  = $secondary_data['availability'];
                   $item->price         = $secondary_data['price'];
                   $item->sell_price    = $secondary_data['sell_price'];

                   // if product saved then save the image and update the link
                   if( $item->save() ):
                       $slug->status = true;
                       $slug->save();
                       Image::make($path)->save($directory_path);
                   endif;

               endif;

               $browser->visit('https://www.classichome.com/customer/account/logout');

           endforeach;

        });
    }
}
