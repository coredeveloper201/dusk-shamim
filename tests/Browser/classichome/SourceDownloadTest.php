<?php

namespace Tests\Browser;

use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use App\Slug as Slug;

class SourceDownloadTest extends DuskTestCase
{
    /**
     * A Dusk test example.
     *
     * @return void
     */
    public function testExample()
    {
        $this->browse(function (Browser $browser) {

            $source_list = [
                'https://www.classichome.com/products/furniture/collections.html?limit=all',
                'https://www.classichome.com/products/furniture/beds.html?limit=all',
                'https://www.classichome.com/products/furniture/bookcases.html?limit=all',
                'https://www.classichome.com/products/furniture/cabinets.html?limit=all',
                'https://www.classichome.com/products/furniture/dressers-nightstands.html?limit=all',
                'https://www.classichome.com/products/furniture/desks.html?limit=all',
                'https://www.classichome.com/products/furniture/some-of-a-kinds.html?limit=all',
                'https://www.classichome.com/products/furniture/seating.html?limit=all',
                'https://www.classichome.com/products/furniture/furniture-set.html?limit=all',
                'https://www.classichome.com/products/furniture/occasional-tables.html?limit=all',
                'https://www.classichome.com/products/furniture/dining-tables-1.html?limit=all',
                'https://www.classichome.com/products/textiles/collections.html?limit=all',
                'https://www.classichome.com/products/textiles/pillows.html?limit=all',
                'https://www.classichome.com/products/textiles/bedding.html?limit=all',
                'https://www.classichome.com/products/textiles/curtains.html?limit=all',
                'https://www.classichome.com/products/textiles/pillow-display.html?limit=all',
                'https://www.classichome.com/products/rugs/collections.html?limit=all',
                'https://www.classichome.com/products/rugs/wool.html?limit=all',
                'https://www.classichome.com/products/rugs/wool-blends-380.html?limit=all',
                'https://www.classichome.com/products/rugs/natural-fiber.html?limit=all',
                'https://www.classichome.com/products/rugs/shag.html?limit=all',
                'https://www.classichome.com/products/rugs/indoor-outdoor.html?limit=all',
                'https://www.classichome.com/products/rugs/viscose.html?limit=all',
                'https://www.classichome.com/products/rugs/doormat.html?limit=all',
                'https://www.classichome.com/products/rugs/rug-displays.html?limit=all',
                'https://www.classichome.com/products/accessories/lighting.html?limit=all',
                'https://www.classichome.com/products/accessories/accents.html?limit=all',
            ];

            foreach($source_list as $source):

                $links = [];
                $product_links = [];

                // browse that link and login
                $browser->visit('https://www.classichome.com/')
                            ->press('a.login')
                            ->type('#mini-login', 'vendors@plaidfox.com')
                            ->type('#mini-password', 'Success7')
                            ->press('div.block-login button.button')
                            ->visit($source)
                            ->assertSee('Products');

                // get all new product links
                foreach($browser->elements('a.product-image') as $element):
                    $links[] = trim($element->getAttribute('href'));
                endforeach;
                // ends

                // gain the product list to import
                if( !empty($links) ):
                    foreach($links as $link):
                        $product_links[] = [
                            'slug' => str_replace( array('.html'), '', trim( parse_url( trim( strip_tags( stripslashes($link) ) ), PHP_URL_PATH ), '/' ) ),
                            'link' => trim( strip_tags( stripslashes($link) ) ),
                        ];
                    endforeach;
                endif;
                // ends

                // save the list to database if not exists
                if( !empty($product_links) ):

                    foreach($product_links as $fields):

                        if( Slug::where('slug', $fields["slug"])->first() ):
                            echo "Reject Record Exists >> " . $fields["slug"] . PHP_EOL;
                            continue;
                        endif;

                        $slug         = new Slug;
                        $slug->slug   = $fields['slug'];
                        $slug->links  = $fields['link'];
                        $slug->status = false;

                        if( $slug->save() ):
                            echo PHP_EOL;
                            echo "Record Added >> ".$fields['slug'];
                            echo PHP_EOL;
                        else:
                            echo "Error >> " . $fields['slug'];
                            echo PHP_EOL;
                        endif;

                    endforeach;

                endif;
                // ends

                $browser->visit('https://www.classichome.com/customer/account/logout');

            endforeach;

        });
    }
}
